#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
COORD coord={0,0};

void _gotoxy(int x, int y)
{
	coord.X=x;
	coord.Y=y;
	SetConsoleCursorPosition(GetStdHandle(	STD_OUTPUT_HANDLE), coord);
}

void _textcolor(int color)
{
    int f=15;
    HANDLE H = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(H, f*1 + color);
}

void _textbackground(int color)
{
   int t=14;
    HANDLE H = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(H, color*16+t);
}

